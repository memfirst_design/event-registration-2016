(function(){
	'use strict';
if(!membersfirst){ 
	var membersfirst = {
		'userFirstName': 'Neighborhood02',
		'userId': 7767244,
		'userLastName': 'AAUser'
	};
}
var memOne = {
	'env':'',
	getViewFromHash:function getViewFromHash(){
		if (window.location.hash.toLowerCase().indexOf('edit') > 0){
			return 'edit';
		} else if(window.location.hash.toLowerCase().indexOf('cancel') > 0){
			return 'cancel';
		} else if(displayData.eventDetails.regstatus.isregistered){
			return 'view' ;
		} else {
				return 'new';
		}
	},
	'thisUser':{'user_id':membersfirst.userId,'name':membersfirst.userFirstName +', '+ membersfirst.userLastName},
	'mountDestination': $('#eventRegistrations'),
	'searchCategories':[{name:'relatives',title:'Relatives'},{name:'buddies',title:'Buddies'},{name:'members',title:'Members'},{name:'guest',title:'Guest'}],
	eventId:function eventId(){
		if(memOne.env == 'dev'){
			return '2232678';
		}
		return window.location.search.toLowerCase().split('cid=')[1].split('&')[0];
	},
	eventUrl:function eventUrl(){return memOne.env == 'dev' ?'/custom/eventReg/eventRegistered.js': '/club/scripts/calendar/calendar_event_json.asp?CID='+memOne.eventId();},
	regDetailUrl:function regDetailUrl(){return memOne.env == 'dev' ? '/custom/eventReg/regDetails.js':'/custom/eventReg/regDetails.js';},
	buddylistListsUrl:function buddylistListsUrl(){ return memOne.env == 'dev'? '/custom/eventReg/buddylistLists.js':'/club/scripts/member/buddylist_api.asp?ACTION=BUDDYLIST_LIST';},
	buddylistMembersUrl:function buddylistMembersUrl(){ return memOne.env == 'dev'? '/custom/eventReg/buddylistMembers.js':'/club/scripts/member/buddylist_api.asp?ACTION=BUDDYLIST_MEMBERS&BLID=';},
	searchDirectoryUrl:function searchDirectoryUrl(){ return memOne.env == 'dev'? '/custom/eventReg/searchDirectory.js':'/club/scripts/member/buddylist_api.asp?ACTION=BUDDY_SEARCH&SRCH_NAME=';},
	formPosturl:function formPostUrl(){return memOne.env == 'dev'? 'https://httpbin.org/post': '';}
};
if(memOne.mountDestination.length){
memOne.env = memOne.mountDestination.hasClass('dev') ? 'dev': 'production';
}else{
memOne.env = 'dev';
}
var apiFunctions = {
	getEventDetails:function getEventDetails(){
		return $.getJSON(memOne.eventUrl());
	},
	getRegDetails:function getRegDetails(){
		return $.get(memOne.regDetailUrl());
	},
	getBuddyLists:function getBuddyLists(){
		return $.get(memOne.buddylistListsUrl());
	},
	getBuddiesFromSpecificList:function getBuddiesFromSpecificList(listId){
		listId = listId || '-1';
		var tempUrl = memOne.env == 'dev' ? memOne.buddylistMembersUrl() : memOne.buddylistMembersUrl() + listId;
		return $.get(tempUrl);
	},
	getFamilyMembers:function getFamilyMembers(){
		return [
				{user_id:9124, name: "Wife, Residents ", title: null},
				{user_id:9033, name: "Son, Residents ", title: null},
				{user_id:9397, name: "Daughter, Residents ", title: null},
				{user_id:287, name: "Father, Residents ", title: null}
			];
	},
	searchMemberDirectory:function searchMemberDirectory(qry){
		var tempUrl = memOne.env == 'dev' ? memOne.searchDirectoryUrl() : memOne.searchDirectoryUrl() + qry;
		return $.get(tempUrl);
	},
	sendRegistration:function sendRegistration(data){
		return $.post(memOne.formPosturl(),data);
	}
};
var utilFunctions = {
	filterUsers: function filterUsers(list,searchString){
		return list.filter(function(item){
			return item.name.split(', ')[0].toLowerCase().indexOf(searchString.toLowerCase()) === 0;
		});
	}
};
var displayData = {
	'directoryMembers':[],
	'currentListOfusers':[],
	'activeSearchTab':'buddies',
	'storedDirectory':false,
	'addedUsers':[],
	'firstLoad':true
};

var buildFunctions = {
	buildHeader:function buildHeader(eventDetails){
		var tempHtml = templates.header;
		var tempDetails = '';
		tempDetails +='<h3 class="text-left appHeading"><a href='+eventDetails.url+'>'+eventDetails.desc+'</a></h3>';
		tempDetails += '<p class="text-left">Date: '+eventDetails.startdate+'</p>';
        if(eventDetails.starttime){
        	tempDetails += '<p class="text-left">Time: '+eventDetails.starttime+'</p>';
        }
        if(eventDetails.categdesc){
	        tempDetails +='<p class="text-left">Category: '+eventDetails.categdesc+'</p>';	        	
        }
        if(eventDetails.memfacname){
        	tempDetails += '<p class="text-left">Microsite: '+eventDetails.memfacname+'</p>';
        }
        tempHtml = tempHtml.replace('{%eventDetails%}',tempDetails);
        var tempNav = '';
        // if(usercanviewothers){
        // 	tempNav +='<a href="" >View Other Registered '+membernamingconfig+'</a>';
        // }
        tempNav += '<a href="" >View My Event Registrations</a>';
        if(eventDetails.regstatus.isregistered && eventDetails.regstatus.canedit == "true"){
        	tempNav += '<a href="#edit" >Edit This Registration</a>';
        }
        if(eventDetails.regstatus.isregistered && eventDetails.regstatus.cancancel == "true"){
        	tempNav += '<a href="#cancel" >Cancel This Registration</a>';
        }
        tempHtml = tempHtml.replace('{%navLinks%}',tempNav);
            
		return tempHtml;
	},
	buildDetails:function buildDetails(eventDetails){
		var tempSection = templates.regDetails;
		var tempSectionDetails = '';
		var tempUserBox = '';
		if (eventDetails.reguser.uregid){
			// add reg user name
			tempSectionDetails += buildFunctions.buildDetailsRow('Registering Resident',eventDetails.reguser.uregname);
		} 
		if (eventDetails.regstatus.confirmationnumber){
			// add confirmation number
			tempSectionDetails += buildFunctions.buildDetailsRow('Confirmation Number:',eventDetails.regstatus.confirmationnumber);
		} 
		/* if (time){
			if (confirmed){
			// add time
			tempSectionDetails += buildFunctions.buildDetailsRow();
			} else {
				add time select

				var select = "<select>";
				for (var i = 0;i <= type.max;i++){
					select += '<option value="'+i+'">'+i+'</option>';
				}
				select+= '</select>';
				tempSectionDetails += buildFunctions.buildDetailsRow(type.name,select);
			}
		} */
		// if need headcount and cannot add others
		if(eventDetails.regconfig.regheadcountbreakout && !eventDetails.regconfig.regothers){
			var arrayOfRegTypes = [{name:'adults',max:'8'},{name:'kids',max:'4'},{name:'highchairs',max:'2'}];
			tempSectionDetails += buildFunctions.buildHeadcountRow(arrayOfRegTypes);	
		}
		
		 if (eventDetails.regconfig.addnotes){
			tempSectionDetails += buildFunctions.buildDetailsRow('Notes:','<textarea></textarea>');
		 }
	
		tempSection = tempSection.replace('{%regDetails%}',tempSectionDetails);
		
		//if new reg
		if(!eventDetails.regstatus.isregistered){
			displayData.addedUsers.push(memOne.thisUser);
		}
		tempUserBox += buildFunctions.buildAttendingList();
		
		tempSection = tempSection.replace('{%usersAdded%}',tempUserBox);
		return tempSection;
	},
	buildAttendingList: function buildAttendingList(){
		var tempUserBox = '';
		var headCount = false;
		var eventDetails = displayData.eventDetails;
		var editSelf = "cannotEdit";
		var editOthers = "cannotEdit"; 
		if (eventDetails.regstatus.canedit =="true" || !eventDetails.regstatus.isregistered/* && cannot edit self*/){
			//if (can edit others){}
			editOthers = "canEdit";
			// if (can edit self){editSelf = "canEdit"}
		}
			
			
		if(displayData.eventDetails.regconfig.regheadcountbreakout == "true" && displayData.eventDetails.regconfig.regothers =="true" ){
			headCount = true;
		}
		displayData.addedUsers.forEach(function(user){
			if(user.user_id == memOne.thisUser.user_id){
				tempUserBox += buildFunctions.buildUser(editSelf,memOne.thisUser,'fa-minus',headCount);
			} else{
				tempUserBox += buildFunctions.buildUser(editOthers,user,'fa-minus',headCount);
			}
		});
		return tempUserBox;
	},
	buildHeadcountRow: function buildHeadcountRow(arrayOfRegTypes){
		var tempHeadcountDetails = '';
		arrayOfRegTypes.forEach(function(type){
				if(!displayData.eventDetails.regstatus.isregistered || memOne.getViewFromHash() == 'edit'){
					 var select = "<select>";
					for (var i = 0;i <= type.max;i++){
						select += '<option value="'+i+'">'+i+'</option>';
					}
					select+= '</select>';
				}else{
					var select = type.count;
				}
				tempHeadcountDetails += buildFunctions.buildDetailsRow(type.name,select);
			});
		return tempHeadcountDetails;
	},
	buildUser: function buildUser(inClass,user,inIcon,headCount){
		var tempHtml = templates.userRow;
		var tempHeadcount = '';
		tempHtml = tempHtml.replace('{%inClass%}',inClass);
		tempHtml = tempHtml.replace('{%userName%}',user.name);
		tempHtml = tempHtml.replace('{%userId%}',user.user_id);
		if (memOne.getViewFromHash() != 'view'){
			tempHtml = tempHtml.replace('{%inIcon%}',inIcon);
		} else{
			tempHtml = tempHtml.replace('{%inIcon%}','');			
		}	
		
		//if user can add headcount per person
		if(headCount){
			if(displayData.eventDetails.regstatus.isregistered && memOne.getViewFromHash() != 'edit'){
				var arrayOfRegTypes = [{name:'Adults',count:'2'},{name:'kids',count:'3'}];
			} else{
				var arrayOfRegTypes = [{name:'adults',max:'8'},{name:'kids',max:'4'},{name:'highchairs',max:'2'}];
			}
			tempHeadcount = buildFunctions.buildHeadcountRow(arrayOfRegTypes);
		
		} 
		tempHtml = tempHtml.replace('{%headCount%}',tempHeadcount);
		
		return tempHtml;
	},
	buildDetailsRow:function buildDetailsRow(title, data){
		var tempRow = templates.regDetailRow;
		tempRow = tempRow.replace('{%rowTitle%}',title);
		tempRow = tempRow.replace('{%rowData%}',data);
		return tempRow;
	},
	buildSearch:function buildSearch(activeTab){
		activeTab = activeTab || "relatives";
		var tempHtml = templates.searchBox;
		var tabContents = '';
		var tempTab = templates.searchTab;
		var inClass = '';
		var sortedCats = memOne.searchCategories;
		var searchVisability = "none";
		var searchFormClass = "hidden";
		/*
		if (cannot add guest){
			sortedCats = sortedCats.map(function(cat){
				if(cat.name !== 'guest'){
					return cat;
				}
		 	});
		}
		if (cannot add buddies){
			sortedCats = sortedCats.map(function(cat){
				if(cat.name !== 'buddies'){
					return cat;
				}
		 	});
		}
		*/
		sortedCats.forEach(function(cat){
			inClass = cat.name;
			if(cat.name === activeTab){
				inClass += ' active';
			}
			var thisTab = tempTab.replace(/{%tabTitle%}/g,cat.title);
			thisTab = thisTab.replace('{%inClass%}',inClass);
			tabContents += thisTab;
		});
		tempHtml = tempHtml.replace('{%searchTabs%}',tabContents);	
		
		var tempFilters = '';
		/* if(filters){
			build filters
		} else */
		tempHtml = tempHtml.replace('{%filters%}',tempFilters);
		
		var tempGuestForm = '';
		if(activeTab == 'guest' /* && can add guest*/){
			tempGuestForm = templates.newGuestForm;
		}
		tempHtml = tempHtml.replace('{%newGuestForm%}',tempGuestForm);
		if (activeTab == 'members'){
			searchFormClass = '';
		}
		tempHtml = tempHtml.replace('{%searchFormClass%}',searchFormClass);
		var users;
		switch(activeTab){
			case 'relatives':
				users = apiFunctions.getFamilyMembers();
				break;
			case 'buddies':
				users = apiFunctions.getBuddiesFromSpecificList();
				break;
			case 'guest':
				users = '';
				break;
			case 'members':
				users = '';
				break;
			default:
				return;
			}
			var searchTemp;
			if (activeTab == 'buddies'){
				users.always(function(response){
					var resp = $.parseJSON(response.responseText);
					displayData.currentListOfusers = resp.users;
					searchTemp = buildFunctions.buildSearchList(resp.users);
					memOne.mountDestination.find('.listOfUserToAdd').html(searchTemp).slideDown('fast');
				});

			} else if(users.length){
				displayData.currentListOfusers = users;
				searchTemp = buildFunctions.buildSearchList(users);	
				tempHtml = tempHtml.replace('{%searchMemberList%}',searchTemp);
				searchVisability = '';
			} else{
				tempHtml = tempHtml.replace('{%searchMemberList%}','');
			}
			tempHtml = tempHtml.replace('{%searchVisability%}',searchVisability);
			return tempHtml;
	},
	buildSearchList: function buildSearchList(users, inClass, inIcon){
		var tempSearchList = '';
		inClass = inClass || '';
		inIcon = inIcon || 'fa-plus';
		users.forEach(function(user){
			tempSearchList += buildFunctions.buildUser(inClass,user,inIcon);
		});
		return tempSearchList;
	},
	buildMessage:function buildMessage(text, styleClass){
		var tempHtml = templates.message;
		tempHtml = tempHtml.replace('{%message%}',text);
		tempHtml = tempHtml.replace('{%messageClass%}',styleClass);
		return tempHtml;
	},
	buildButtons:function buildButtons(){
		var tempHtml = templates.buttons;

		return tempHtml;
	},
	buildPage:function buildPage(){
		var rawEventDetails = apiFunctions.getEventDetails(memOne.eventId());
		displayData.addedUsers = [];
		rawEventDetails.done(function(response){
			displayData.eventDetails = response.event;
			var tempHtml = buildFunctions.buildHeader(displayData.eventDetails);
			tempHtml += buildFunctions.buildDetails(displayData.eventDetails);
			if(displayData.eventDetails.regstatus.isregistered == 'true' && memOne.getViewFromHash() == 'view' && displayData.eventDetails.regconfig.message){
				tempHtml += buildFunctions.buildMessage(displayData.eventDetails.regconfig.message, '');
			}
			if(displayData.eventDetails.regstatus.isregistered == 'true' && memOne.getViewFromHash() == 'cancel' && displayData.eventDetails.regconfig.message){
				tempHtml += buildFunctions.buildMessage('<p>Are you sure you want to cancel this reservation?</p> <a href="#cancel" id="cancelThisReservation">Yes I\'m sure</a>', '');
			}
			if(displayData.eventDetails.regconfig.regothers == "true" && (memOne.getViewFromHash() == 'new' || memOne.getViewFromHash() == 'edit')){
				tempHtml += buildFunctions.buildSearch();
			}
			if(memOne.getViewFromHash() == 'new' || memOne.getViewFromHash() == 'edit' ){
				tempHtml += buildFunctions.buildButtons();
			}
			memOne.mountDestination.html(tempHtml);
		}).fail(function(response,error,details){
			var tempHtml = buildFunctions.buildMessage('Sorry, an error occured: '+ error+' try refreshing.','bg-danger');
			console.log(details);
			memOne.mountDestination.html(tempHtml);
		});
		if(displayData.firstLoad){
			buildFunctions.attachEvents();
			displayData.firstLoad = false;
		}
	},
	attachEvents:function attachEvents(){
		memOne.mountDestination.on('click','.searchBox .nav-tabs a',eventFunctions.changeTab);
		memOne.mountDestination.on('keyup','input',eventFunctions.searchKeyup);	
		memOne.mountDestination.on('click','#searchList div.userRow',eventFunctions.clickUser);
		memOne.mountDestination.on('click','#addedUsers div.userRow i',eventFunctions.clickUser);
		memOne.mountDestination.on('click','.sendButtons button[type=submit]',eventFunctions.sendForm);
		memOne.mountDestination.on('click','.sendButtons button[type=cancel]',eventFunctions.cancelForm);
		memOne.mountDestination.on('click','.sendButtons button[type=clear]',eventFunctions.clearForm);
		memOne.mountDestination.on('click','#cancelThisReservation',eventFunctions.cancelReservation);

		$(window).on('hashchange',buildFunctions.buildPage);
	}


};
var eventFunctions = {
	changeTab:function changeTab(event){
		event.preventDefault();
		displayData.activeSearchTab = $(this).attr('id').toLowerCase();
		var tempHtml = buildFunctions.buildSearch(displayData.activeSearchTab);
		memOne.mountDestination.find('.searchBox').replaceWith(tempHtml);
	},
	searchKeyup: function searchKeyup(){
		var query = $(this).val();
		var tempHtml ='';
		if(query.length > 2){
			if (!displayData.directoryMembers.length){
				apiFunctions.searchMemberDirectory(query).always(function(response){
					displayData.directoryMembers = $.parseJSON(response.responseText).users;
					displayData.currentListOfusers = displayData.directoryMembers;
					tempHtml = buildFunctions.buildSearchList(displayData.directoryMembers);
					memOne.mountDestination.find('.listOfUserToAdd').html(tempHtml).slideDown();
				});
			} else {
				var users = utilFunctions.filterUsers(displayData.directoryMembers,query);
				displayData.currentListOfusers = users;
				tempHtml = buildFunctions.buildSearchList(users);
				memOne.mountDestination.find('.listOfUserToAdd').html(tempHtml).slideDown();
			}
			
		} else {
			displayData.directoryMembers = [];
			memOne.mountDestination.find('.listOfUserToAdd').html('').hide();
		}
	},
	clickUser:function clickUser(){
		var clickedUserId = $(this).attr('data-uid')?$(this).attr('data-uid'):$(this).parents('.userRow').attr('data-uid');
		var isAddedIndex;
		var isAdded = displayData.addedUsers.filter(function(user,index){
			if(user.user_id == clickedUserId){
				isAddedIndex = index;
			}
			return user.user_id == clickedUserId;
		});
		if (isAdded.length) {
			displayData.addedUsers.splice(isAddedIndex,1);
		} else{
			var newAddedUser = displayData.currentListOfusers.filter(function(user){
				return user.user_id == clickedUserId;
			});
			displayData.addedUsers.push(newAddedUser[0]);
		}
		var tempHtml = buildFunctions.buildAttendingList();
		memOne.mountDestination.find('#addedUsers').html(tempHtml);
	},
	sendForm: function sendForm(){
		var data = {
			'user':memOne.thisUser.user_id,
			'invitedUsers':displayData.addedUsers
		};
		if(displayData.eventDetails.regconfig.addnotes){
			data.notes = memOne.mountDestination.find('textarea').val();
		}
		console.log(data);
		apiFunctions.sendRegistration(data)
		.success(function(response){
			console.log('success ');
			console.dir(response);
			buildFunctions.buildPage();
		})
		.error(function(response){
			console.log('error '+response);
			var message = buildFunctions.buildMessage('Oops, something went wrong!','bg-danger');
			if(memOne.mountDestination.find('.mrm-message').length){
				memOne.mountDestination.find('.mrm-message').replaceWith(message);
			} else {
				memOne.mountDestination.prepend(message);
			}
		});
	},
	cancelForm: function cancelForm(){
		history.go(-1);
	},
	clearForm: function clearForm(){
		buildFunctions.buildPage();
	},
	cancelReservation:function cancelReservation(){
		// send cancel to API
	}

};
var templates = {
	'message':'<div class="row {%messageClass%} eventBox mrm-message" >{%message%}</div>',
	'header':'<div class="row eventDetails eventBox"><div class="col-sm-7 col-xs-12">{%eventDetails%}</div><div class="col-xs-12 col-sm-5 eventNavigation">{%navLinks%}</div></div>',
	'regDetails':'<div class="row registrationDetails eventBox" ><h4>Registration Details</h4>{%regDetails%}<div class="row addedMembers" ><div class="col-xs-4">People Attending:</div><div class="col-xs-8" id="addedUsers">{%usersAdded%}</div></div></div>',
	'regDetailRow':'<div class="row registrationDetailRow" ><div class="col-xs-4">{%rowTitle%}</div><div class="col-xs-8">{%rowData%}</div></div>',
	'userRow':'<div class="row userRow {%inClass%}" data-uid="{%userId%}"><div class="col-xs-12 nameRow"><i class="fa {%inIcon%}"></i> {%userName%}</div><div class="col-xs-12 headCount">{%headCount%}</div></div>',
	'filter':'<div class="form-group"><label for="{%filterName%}">{%filterName%}</label><select name="{%filterName%}">{%filterList%}</select></div>',
	'searchTab': '<li class="{%inClass%}" ><a href="#" id="{%tabTitle%}">{%tabTitle%}</a></li>',
	'searchBox': '<div class="row searchBox eventBox"><h4>Register the people joining you.</h4><ul class="nav nav-tabs nav-justified">{%searchTabs%}</ul><div class="content-panel"><div class="row filters">{%filters%}</div><div class="form-group {%searchFormClass%}" ><label for="searchInput">Search by Last Name</label><input name="searchInput" ref="searchInput" /></div><div class="listOfUserToAdd " style="display:{%searchVisability%}" id="searchList">{%searchMemberList%}</div>{%newGuestForm%}</div></div>',
	'buttons': '<div class="col-xs-12 sendButtons"><button class="btn btn-primary" type="submit">Send</button><button class="btn btn-primary" type="cancel">Cancel</button><button class="btn btn-primary" type="clear">Clear</button></div>',
	'newGuestForm': '<div class="newGuest"><form>'+
		'<div class="form-group clearfix"><label for="guestFirstName" class="col-xs-12">Guests first name: </label><input name="guestFirstName" ref= "guestFirstName" class="col-xs-12 col-sm-8 col-md-6"/></div>'+
		'<div class="form-group clearfix"><label for="guestLastName" class="col-xs-12">Guests last name: </label><input name="guestLastName" ref= "guestLastName" class="col-xs-12 col-sm-8 col-md-6"/></div>' +
		'<div class="form-group clearfix"><label for="guestEmail" class="col-xs-12">Guests Email: </label><input name="guestEmail" ref="guestEmail"  class="col-xs-12 col-sm-8 col-md-6"/></div>' +
		'<div class="form-group clearfix"><label for="guestsPhone" class="col-xs-12">Guests Phone Number: </label><input name="guestsPhone" ref="guestsPhone"  class="col-xs-12 col-sm-8 col-md-6"/></div>' +
		'<div class="form-group clearfix"><button type="submit" class="btn btn-primary">Add New Guest</button><button type="reset" class="btn btn-warning">Clear</button></div></form></div>'
};
$(document).ready(buildFunctions.buildPage);
window.memfirstEvent = {
	templates:templates,
	buildFunctions:buildFunctions,
	eventFunctions:eventFunctions,
	utilFunctions:utilFunctions,
	displayData:displayData,
	memOne:memOne,
	apiFunctions:apiFunctions
};
})();
