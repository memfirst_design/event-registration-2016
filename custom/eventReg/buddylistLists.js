{
	"buddylists":[
		{"blid":-1,"name":"ALL MY BUDDIES","count":30},
		{"blid":136,"name":"teset2","count":8},
		{"blid":135,"name":"test","count":8},
		{"blid":137,"name":"test3","count":8},
		{"blid":138,"name":"test4","count":6}
	],
	"status":0,
	"status_msg":"",
	"id":null,
	"action":"BUDDYLIST_LIST",
	"count":5,
	"log":null,
	"config":[
		{
			"buddylistheading":"BUDDY LIST",
			"buddylistheadingplural":"MY BUDDY LISTS",
			"userheading":"Member",
			"userheadingplural":"Members",
			"maxusersperbuddylist":25,
			"maxbuddylists":10,
			"showemail":true,
			"showaltemail":true,
			"displayphone":null
		}
	]
}