{
	"users":[
		{"user_id":9085,"name":"Caldwell, Jean ","title":null,"firstname":"Jean","middlename":null,"lastname":"Caldwell","sufffix":null,"familiarname":"","email":"429085@memfirsttest.net","altemail":"429085_a@memfirsttest.net"},
		{"user_id":3161,"name":"Caldwell, Marsha ","title":null,"firstname":"Marsha","middlename":null,"lastname":"Caldwell","sufffix":null,"familiarname":"","email":"443161@memfirsttest.net","altemail":"443161_a@memfirsttest.net"},
		{"user_id":7890,"name":"Camel, Lisa ","title":null,"firstname":"Lisa","middlename":null,"lastname":"Camel","sufffix":null,"familiarname":"","email":"577890@memfirsttest.net","altemail":"577890_a@memfirsttest.net"},
		{"user_id":2103,"name":"Cammack, Maxyne ","title":null,"firstname":"Maxyne","middlename":null,"lastname":"Cammack","sufffix":null,"familiarname":"","email":"432103@memfirsttest.net","altemail":""},
		{"user_id":51374,"name":"Campbell, Connie ","title":null,"firstname":"Connie","middlename":null,"lastname":"Campbell","sufffix":null,"familiarname":"","email":"1051374@memfirsttest.net","altemail":"1051374_a@memfirsttest.net"},
		{"user_id":3940,"name":"Campbell, Hugh ","title":null,"firstname":"Hugh","middlename":null,"lastname":"Campbell","sufffix":null,"familiarname":"","email":"433940@memfirsttest.net","altemail":"433940_a@memfirsttest.net"}
	],
	"status":0,
	"status_msg":"Returned Member for BUDDY LIST: 138:test4",
	"id":null,
	"action":"BUDDYLIST_MEMBERS",
	"count":6,
	"log":"",
	"config":[
		{
			"buddylistheading":"BUDDY LIST",
			"buddylistheadingplural":"MY BUDDY LISTS",
			"userheading":"Member",
			"userheadingplural":"Members",
			"maxusersperbuddylist":25,
			"maxbuddylists":10,
			"showemail":true,
			"showaltemail":true,
			"displayphone":null
		}
	]
}