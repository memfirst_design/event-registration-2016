'use strict';
var templates = memfirstEvent.templates;
var buildFunctions = memfirstEvent.buildFunctions;
var eventFunctions = memfirstEvent.eventFunctions;
var utilFunctions = memfirstEvent.utilFunctions;
var displayData = memfirstEvent.displayData;
var memOne = memfirstEvent.memOne;
var apiFunctions = memfirstEvent.apiFunctions;
window.location.hash = '';

QUnit.test('assert environment',function(assert){
	assert.equal(memOne.env,'dev','Environment is dev');
	displayData.eventDetails.regstatus.isregistered = false;
	assert.equal(memOne.getViewFromHash(),'new','Router returns new if no has present');
	displayData.eventDetails.regstatus.isregistered = true;
	assert.equal(memOne.getViewFromHash(),'view','Router returns view if no hash and is registered');
	window.location.hash = "#edit";
	assert.equal(memOne.getViewFromHash(),'edit','Router returns edit if hash hash is edit');
	window.location.hash = "#cancel";
	assert.equal(memOne.getViewFromHash(),'cancel','Router returns cancel if hash is cancel');
	window.location.hash = '';
	assert.ok(memOne.mountDestination,'Mount destination exists');
	assert.ok(memOne.eventId(), 'Event Id returns a number');
});
QUnit.test('utility functions',function(assert){
	assert.deepEqual(utilFunctions.filterUsers([{name:'bob'},{name:'jim'}],'bob'),[{name:'bob'}],'Filter should return a single matching user from a list with one match');
	assert.deepEqual(utilFunctions.filterUsers([{name:'bob'},{name:'jim'},{name:'bob,test'}],'bob'),[{name:'bob'},{name:'bob,test'}],'Filter should return matching usesr from a list with two matches,one with a comma');
});
QUnit.test('templates',function(assert){
	assert.ok(templates.message,'message templates exists');
	assert.ok(templates.header, 'header templates exists');
	assert.ok(templates.regDetails, 'registration details templates exists');
	assert.ok(templates.regDetailRow, 'registration details row templates exists');
	assert.ok(templates.userRow, ' user row templates exists');
	assert.ok(templates.filter,'filter templates exists');
	assert.ok(templates.searchTab, 'search tab templates exists');
	assert.ok(templates.searchBox, 'search box templates exists');
	assert.ok(templates.buttons,'buttons templates exists');
	assert.ok(templates.newGuestForm, 'new guest form templates exists');
});
QUnit.test('build functions',function(assert){
	var arrayOfRegTypes = [{name:'Adults',max:'2'},{name:'kids',max:'3'}];
	displayData.eventDetails.regstatus.isregistered = false;
	assert.equal(buildFunctions.buildHeadcountRow(arrayOfRegTypes), '<div class="row registrationDetailRow" ><div class="col-xs-4">Adults</div><div class="col-xs-8"><select><option value="0">0</option><option value="1">1</option><option value="2">2</option></select></div></div><div class="row registrationDetailRow" ><div class="col-xs-4">kids</div><div class="col-xs-8"><select><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option></select></div></div>', 'build headcount builds correct string for not registered');

	var arrayOfRegTypes = [{name:'Adults',count:'2'},{name:'kids',count:'3'}];
	displayData.eventDetails.regstatus.isregistered = true;
	assert.equal(buildFunctions.buildHeadcountRow(arrayOfRegTypes), '<div class=\"row registrationDetailRow\" ><div class=\"col-xs-4\">Adults</div><div class=\"col-xs-8\">2</div></div><div class=\"row registrationDetailRow\" ><div class=\"col-xs-4\">kids</div><div class=\"col-xs-8\">3</div></div>', 'build headcount builds correct string for viewing registered');

	assert.equal(buildFunctions.buildMessage('text', 'styleClass'),'<div class="row styleClass eventBox mrm-message" >text</div>', 'Build message function returns correct string');

	assert.equal(buildFunctions.buildDetailsRow('text', 'styleClass'),'<div class="row registrationDetailRow" ><div class="col-xs-4">text</div><div class="col-xs-8">styleClass</div></div>', 'Build details row function returns correct string');

	window.location.hash = '#edit';
	assert.equal(buildFunctions.buildSearchList([{user_id:1234,name:'Larry Smith'},{user_id:1234,name:'Larry Smith'}], 'inClass', 'inIcon'),'<div class="row userRow inClass" data-uid="1234"><div class="col-xs-12 nameRow"><i class="fa inIcon"></i> Larry Smith</div><div class="col-xs-12 headCount"></div></div><div class="row userRow inClass" data-uid="1234"><div class="col-xs-12 nameRow"><i class="fa inIcon"></i> Larry Smith</div><div class="col-xs-12 headCount"></div></div>', 'Build searchlist returns correct string');

	assert.equal(buildFunctions.buildUser('inClass',{user_id:1234,name:'Larry Smith'}, 'inIcon',false),'<div class="row userRow inClass" data-uid="1234"><div class="col-xs-12 nameRow"><i class="fa inIcon"></i> Larry Smith</div><div class="col-xs-12 headCount"></div></div>', 'Build user function returns correct string');

	assert.equal(buildFunctions.buildUser('inClass',{user_id:1234,name:'Larry Smith'}, 'inIcon',true),'<div class=\"row userRow inClass\" data-uid=\"1234\"><div class=\"col-xs-12 nameRow\"><i class=\"fa inIcon\"></i> Larry Smith</div><div class=\"col-xs-12 headCount\"><div class=\"row registrationDetailRow\" ><div class=\"col-xs-4\">adults</div><div class=\"col-xs-8\"><select><option value=\"0\">0</option><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option></select></div></div><div class=\"row registrationDetailRow\" ><div class=\"col-xs-4\">kids</div><div class=\"col-xs-8\"><select><option value=\"0\">0</option><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option></select></div></div><div class=\"row registrationDetailRow\" ><div class=\"col-xs-4\">highchairs</div><div class=\"col-xs-8\"><select><option value=\"0\">0</option><option value=\"1\">1</option><option value=\"2\">2</option></select></div></div></div></div>', 'Build user function with headcount returns correct string');


});

